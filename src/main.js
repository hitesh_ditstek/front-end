import { createApp} from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import "bootstrap"
import "bootstrap/dist/css/bootstrap.min.css"
import 'jquery/dist/jquery.min.js'
import { defineComponent } from 'vue'
const app = createApp(App)
app.config.errorHandler = () => null
app.use(store).use(router).use(defineComponent).mount('#app')
