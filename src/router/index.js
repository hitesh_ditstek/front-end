import { createRouter, createWebHashHistory } from 'vue-router'
import Dashboard from "../views/dashboard/Dashboard.vue"
import Login from "../views/login/Login.vue"
import AppointmentCalendar from "../views/appointment-calendar/AppointmentCalendar.vue"
import Communications from "../views/communications/Communications.vue"
import CareCoordinator from "../views/care_coordinator/CareCoordinator.vue"
import ManagePatients from "../views/manage_patients/ManagePatients.vue"
import PatientSummary from "../views/manage_patients/PatientSummary.vue"
const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/appointment-calendar',
    name: 'AppointmentCalendar',
    component: AppointmentCalendar
  },
  {
    path: '/messages',
    name: 'Manage Messages',
    component: Communications
  },
  {
    path: '/care_coordinator',
    name: 'Manage Care Coordinator',
    component: CareCoordinator
  },
  {
    path: '/manage_patients',
    name: 'Manage Patients',
    component: ManagePatients
  },
  {
    path: '/patients_summary/:id',
    name: 'Patients Summary',
    component: PatientSummary
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
