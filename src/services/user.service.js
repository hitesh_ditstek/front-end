import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://ditstekdemo.com/Virtare/public/api/';

class UserService {
  getPublicContent() {
    return axios.get(API_URL + 'all');
  }

  getUserBoard() {
    return axios.get(API_URL + 'staff/patient', { headers: authHeader() });
  }

}

export default new UserService();